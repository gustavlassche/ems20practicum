#include <stdio.h>

int main(void)
{
    int i = 51966;
    char c = '?';
    double d = 3.141592653;
    char s[] = "Dit is een string!";

    printf("[Cortex_M4_O]\n");
    printf("De variabele notatie is dit: %d\n", i);
    printf("In hexadecimale notatie is dit: %x\n", i);
    printf("De variabele c heeft de waarde: %c\n", c);
    printf("De variabele d heeft de waarde: %f\n", d);
    printf("De variabele s heeft de waarde: %s\n", s);

    while (1);
    return 0;
}
